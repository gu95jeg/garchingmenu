import urllib
import datetime
import os

from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfpage import PDFTextExtractionNotAllowed
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.layout import LAParams
from pdfminer.converter import PDFPageAggregator


class Dish(object):
    def __init__(self, text="", name="N/A", price="N/A"):
        self.text = text
        self.name = name
        self.price = price


def download_menu_pdf(url, path):
    # Save this week's menu as pdf
    pdffile = urllib.urlopen(url)
    output = open(path, 'wb')
    output.write(pdffile.read())
    output.close()


def get_colrightborders(textbox_l):
    colrightborders = []
    colmiddle = []
    days = [u"Montag", u"Dienstag", u"Mittwoch", u"Donnerstag", u"Freitag"]
    done = [0, 0, 0, 0, 0]
    # get column middle positions
    for textbox in textbox_l:
        for d in range(len(days)):
            if days[d] in textbox.get_text() and not done[d]:
                colmiddle.append(0.5 * (textbox.x1 + textbox.x0))
                # mark as done to prevent multiple counts when the document has multiple pages
                done[d] = 1
    colmiddle.sort()
    # get column right borders
    colrightborders.append(colmiddle[0] - 0.5 * (colmiddle[1] - colmiddle[0]))
    colrightborders += [colmiddle[i] + 0.5 * (colmiddle[i + 1] - colmiddle[i]) for i in range(0, len(colmiddle) - 1)]
    colrightborders.append(colmiddle[-1] + 0.5 * (colmiddle[-1] - colmiddle[-2]))

    return colrightborders


def get_rowlowborders(textbox_l):
    rowlowborders = []
    rowmiddles = []
    rowlabels = []
    categories = [u"Veggie", u"Traditionelle", u"Internationale", u"Specials", u"Beilagen",
                  u"die Milch machts", u"Desserts"]
    # get row middle positions
    for textbox in textbox_l:
        for c in range(len(categories)):
            if categories[c] in textbox.get_text():
                rowmiddles.append(0.5 * (textbox.y1 + textbox.y0))
                rowlabels.append(textbox.get_text())
    rowlabels_sorted = [label for (middle, label) in sorted(zip(rowmiddles, rowlabels))]
    rowmiddles.sort()
    # get column right borders
    rowlowborders.append(rowmiddles[0] - 0.5 * (rowmiddles[1] - rowmiddles[0]))
    rowlowborders += [rowmiddles[i] + 0.5 * (rowmiddles[i + 1] - rowmiddles[i]) for i in range(0, len(rowmiddles) - 1)]
    rowlowborders.append(rowmiddles[-1] + 0.5 * (rowmiddles[-1] - rowmiddles[-2]))
    return rowlowborders, rowlabels_sorted


def get_dish_grid(textbox_l, colrightborders, rowlowborders):
    # generate grid of dishes
    numrows = len(rowlowborders) - 1
    numcolums = len(colrightborders) - 1
    dishgrid = [[Dish() for row in range(numrows)] for column in range(numcolums)]
    # iterate through grid
    for y in range(numrows):
        for x in range(numcolums):
            # find all textboxes in current grid position
            boxes = []
            for textbox in textbox_l:
                if (textbox.x0 > colrightborders[x] and textbox.x1 < colrightborders[x + 1]
                        and textbox.y0 > rowlowborders[y] and textbox.y1 < rowlowborders[y + 1]):
                    boxes.append(textbox)
            ypos_l = []
            # sort textboxes
            for textbox in boxes:
                ypos_l.append(textbox.y0)
            sorted_boxes = [box for (ypos, box) in sorted(zip(ypos_l, boxes), key=lambda y: y[0])]
            sorted_boxes.reverse()
            # store texts in a 2d array
            for textbox in sorted_boxes:
                dishgrid[x][-y - 1].text += textbox.get_text()
    return dishgrid


def menu_today(dishgrid):
    # print today's menu
    weekday = datetime.datetime.today().weekday()
    for dish in range(len(dishgrid[weekday])):
        print(dishgrid[weekday][dish].text)


def parse_menu(path):
    # generate a grid of dish objects for every page in the document
    dishgrid_l = []
    # Parse PDF:
    fp = open(path, 'rb')
    # # Create a PDF parser object associated with the file object.
    parser = PDFParser(fp)
    # # Create a PDF document object that stores the document structure.
    document = PDFDocument(parser)
    # # Check if the document allows text extraction. If not, abort.
    if not document.is_extractable:
        raise PDFTextExtractionNotAllowed
    rsrcmgr = PDFResourceManager()
    # # Set parameters for analysis.
    laparams = LAParams()
    # # Create a PDF page aggregator object.
    device = PDFPageAggregator(rsrcmgr, laparams=laparams)
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    for page in PDFPage.create_pages(document):
        interpreter.process_page(page)
        # receive the LTPage layout object for the page.
        layout = device.get_result()
        # extract textboxes
        textbox_l = [obj for obj in layout._objs if obj.__class__.__name__ == "LTTextBoxHorizontal"]
        # get column borders through day label center positions
        colrightborders = get_colrightborders(textbox_l)
        # get row borders through horizontal rectangles (tabular separators) in the first column
        # # get all rectangles with x0 in the first column
        rect_l = [rect for rect in layout._objs if "Rect" in rect.__class__.__name__ and rect.x0 < colrightborders[0]]
        # # get row borders by width/height and position check
        rowlowborders = []
        for rect in rect_l:
            if rect.height < 8 and rect.width > 30 and rect.x0 < 30:
                rowlowborders.append(rect.y0)
        rowlowborders.sort()
        # generate grid with Dish objects
        dishgrid_l.append(get_dish_grid(textbox_l, colrightborders, rowlowborders))
    return dishgrid_l

# Main
# # Settings
url = "https://www.ipp.mpg.de/4241085/"
pdfdir = "./"
pdfpath = pdfdir + 'current_menu.pdf'
if not os.path.exists(pdfdir):
    os.makedirs(pdfdir)
# # Parse dishes from url
download_menu_pdf(url, pdfpath)
dishgrid_l = parse_menu(pdfpath)
# # TODO extract dish categories, names and prices and save them in a database
# # Print today's menu
for dishgrid in dishgrid_l:
    menu_today(dishgrid)
